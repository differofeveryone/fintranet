import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../+state/reducer';
import { uploadImage } from '../+state/action';

@Component({
  selector: 'app-image-upload',
  templateUrl: './image-upload.component.html',
  styleUrls: ['./image-upload.component.scss'],
})
export class ImageUploadComponent implements OnInit {
  image = '';

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {}

  onFileChange(event: any) {
    if (event.target.files && event.target.files.length) {
      const file: File = event.target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(file);

      reader.onload = () => {
        const img = new Image();
        img.src = reader.result as string;
        let height: number;
        let width: number;
        img.onload = async () => {
          height = img.naturalHeight;
          width = img.naturalWidth;
          this.image = reader.result as string;
          this.store.dispatch(uploadImage({ image: this.image }));
        };
      };
    }
  }
}
