import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../+state/reducer';
import { selectForm, selectImage, selectTableRow } from '../+state/selector';

@Component({
  selector: 'app-summery',
  templateUrl: './summery.component.html',
  styleUrls: ['./summery.component.scss'],
})
export class SummeryComponent implements OnInit {
  image$ = this.store.select(selectImage);
  form$ = this.store.select(selectForm);
  tableRow$ = this.store.select(selectTableRow);

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {}
}
