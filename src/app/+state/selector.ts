import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AppState } from './reducer';

export const select = createFeatureSelector<AppState>('app');

export const selectImage = createSelector(select, (state) => state.image);

export const selectForm = createSelector(
  select,
  (state: AppState) => state.form
);

export const selectTableRow = createSelector(
  select,
  (state: AppState) => state.tableRow
);
