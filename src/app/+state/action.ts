import { createAction, props } from '@ngrx/store';
import { TableData } from '../table/model/table-data.model';
import { PartialFormValue } from '../form/model/form-value.model';

export const uploadImage = createAction(
  '[App] upload image',
  props<{ image: string }>()
);

export const submitForm = createAction(
  '[App] submit form',
  props<{ form: PartialFormValue }>()
);

export const selectPerson = createAction(
  '[App] select person',
  props<{ tableRow: TableData }>()
);
