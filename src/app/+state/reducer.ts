import { Action, createReducer, on } from '@ngrx/store';
import { TableData } from '../table/model/table-data.model';
import { selectPerson, submitForm, uploadImage } from './action';
import { PartialFormValue } from '../form/model/form-value.model';

export interface AppState {
  image: string | null;
  form: PartialFormValue | null;
  tableRow: TableData | null;
}

const initialState = {
  image: null,
  form: null,
  tableRow: null,
} as AppState;

const appReducer = createReducer(
  initialState,
  on(uploadImage, (state, { image }) => {
    return {
      ...state,
      image,
    };
  }),
  on(submitForm, (state, { form }) => {
    console.log(form);
    return {
      ...state,
      form,
    };
  }),
  on(selectPerson, (state, { tableRow }) => {
    return {
      ...state,
      tableRow,
    };
  })
);

export function reducer(state: any, action: Action) {
  return appReducer(state, action);
}
