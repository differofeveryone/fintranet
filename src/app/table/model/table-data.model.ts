export interface TableData {
  name: string;
  lastName: string;
  email: string;
  birthdate: string;
}
