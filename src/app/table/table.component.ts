import { Component, OnInit } from '@angular/core';
import { TableColumn } from './model/table-column.model';
import { TableData } from './model/table-data.model';
import { Store } from '@ngrx/store';
import { AppState } from '../+state/reducer';
import { selectPerson } from '../+state/action';

@Component({
  selector: 'app-table-step',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit {
  cols: TableColumn[] = [];
  data: TableData[] = [];

  selectedItem: TableData | undefined;

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.cols = [
      { field: 'name', header: 'Name' },
      { field: 'lastName', header: 'Last Name' },
      { field: 'email', header: 'Email' },
      { field: 'birthdate', header: 'Birthdate' },
    ];

    this.data = [
      {
        name: 'John',
        lastName: 'White',
        email: 'john2012@hotmail.com',
        birthdate: '5/4/1987',
      },
      {
        name: 'Ed',
        lastName: 'Walker',
        email: 'edwalker@gmail.com',
        birthdate: '9/12/1970',
      },
    ];
  }

  onSelectItem() {
    this.store.dispatch(selectPerson({ tableRow: this.selectedItem! }));
  }
}
