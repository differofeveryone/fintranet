interface FormValue {
  amount: number | null;
  date: Date | null;
  status: string | null;
  sourceOfFund: string | null;
}

export type PartialFormValue = Partial<FormValue>;
