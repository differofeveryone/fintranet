import { FormControl } from '@angular/forms';

export interface FormModel {
  amount: FormControl<number | null>;
  date: FormControl<Date | null>;
  status: FormControl<string | null>;
  sourceOfFund: FormControl<string | null>;
}
