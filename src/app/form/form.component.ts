import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '../+state/reducer';
import { submitForm } from '../+state/action';
import { FormModel } from './model/form.model';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnInit {
  form: FormGroup<FormModel>;

  date = new Date();
  maxDate = new Date(new Date().setDate(new Date().getDate() + 5));

  dropdownOptions: string[] = ['Approve', 'Reject', 'Prefer', 'Register'];

  constructor(private store: Store<AppState>) {
    this.form = new FormGroup<FormModel>({
      amount: new FormControl(null, { validators: Validators.required }),
      date: new FormControl(new Date(), { validators: Validators.required }),
      status: new FormControl(null, { validators: Validators.required }),
      sourceOfFund: new FormControl(null, {
        validators: [this.letterValidator, Validators.required],
      }),
    });
  }

  ngOnInit(): void {}

  private letterValidator: ValidatorFn = (
    control: AbstractControl<string>
  ): ValidationErrors | null => {
    const pattern = /[^a-zA-Z]/g;
    if (!control.value) return null;
    const match = control.value.match(pattern);
    if (match?.length && control.value.length) {
      control.setValue(control.value.replace(/[^a-zA-Z]/g, ''));
    }
    return null;
  };

  submit() {
    if (this.form.invalid) {
      return;
    }
    this.store.dispatch(submitForm({ form: this.form.value }));
  }
}
